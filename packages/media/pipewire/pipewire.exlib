# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=PipeWire tag=${PV} ]
require meson

SUMMARY="PipeWire is a project that aims to greatly improve handling of audio and video under Linux"

HOMEPAGE="https://pipewire.org/"

LICENCES="LGPL-2.1"
SLOT="0"

MYOPTIONS="
    doc
    gstreamer [[ description = [ build Pipewire's GStreamer plugins ] ]]
    systemd
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-text/xmltoman
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        media/ffmpeg [[ note = [ automagic ] ]]
        media-libs/SDL:2 [[ note = [ automagic ] ]]
        sys-apps/dbus
        sys-sound/alsa-lib
        x11-libs/libX11
        gstreamer? (
            dev-libs/glib:2[>=2.32.0]
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        systemd? ( sys-apps/systemd )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dman=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc docs'
    'gstreamer gstreamer enabled disabled'
    systemd
)

