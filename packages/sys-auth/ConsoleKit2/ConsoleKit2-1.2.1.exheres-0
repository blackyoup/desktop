# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2010 Brett Witherspoon <spoonb@cdspooner.com>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Purpose License v2

require systemd-service pam
require openrc-service
require github [ release=${PV} suffix=tar.bz2 ]

SUMMARY="A framework for defining and tracking users, login sessions, and seats"

UPSTREAM_DOCUMENTATION="https://consolekit2.github.io/ConsoleKit2/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cgroups
    debug
    doc
    polkit
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxml2:* [[ note = [ for xmllint ] ]]
        sys-devel/gettext[>=0.19]
        doc? (
            app-text/docbook-xml-dtd:4.1.2
            app-text/xmlto
        )
    build+run:
        dev-libs/glib:2[>=2.40]
        sys-apps/acl
        sys-apps/dbus[>=0.82]
        sys-libs/pam
        sys-libs/zlib
        x11-libs/libX11[>=1.0]
        cgroups? (
            sys-apps/cgmanager
            sys-libs/libnih
        )
        polkit? ( sys-auth/polkit:1[>=0.92] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        sys-power/pm-utils [[ description = [ Needed for suspend, hibernation, and hybrid sleep support ] ]]
        !sys-auth/ConsoleKit [[
            description = [ ${PN} is a fork of the now unmaintained ConsoleKit, which provides the same libraries and is maintained ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ac_cv_file__sys_class_tty_tty0_active=true # fix cross compilation
    --localstatedir=/var
    --with-rundir=/run
    --with-pam-module-dir=$(getpam_mod_dir)
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --enable-pam-module
    --enable-udev-acl
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'cgroups libcgmanager'
    debug
    'doc docbook-docs'
    polkit
)

# seems to be broken with dbus related xml crap...
RESTRICT=test

DEFAULT_SRC_CONFIGURE_TESTS=(
    '--recommended --enable-tests --disable-tests'
)

src_install() {
    default

    # CK_SEAT_DIR, CK_SESSION_DIR
    keepdir /usr/$(exhost --target)/lib/ConsoleKit/run-{seat,session}.d
    keepdir /etc/ConsoleKit/run-{seat,session}.d

    # CK_LOG_FILE
    keepdir /var/log/ConsoleKit

    exeinto /etc/ConsoleKit/run-session.d
    doexe "${FILES}"/pam-foreground-compat.ck

    # ConsoleKit creates this directory when needed
    edo rmdir "${IMAGE}"/run{/ConsoleKit,}

    install_openrc_files
}

pkg_postinst() {
    # Make sure we have the correct permissions on initial install
    [[ -e "${ROOT}/var/log/ConsoleKit/history" ]] || touch "${IMAGE}/var/log/ConsoleKit/history"

    elog "If you have problems creating local sessions be sure you have AUDIT"
    elog "and AUDITSYSCALL enabled in your kernel and you are using a login"
    elog "application that has ConsoleKit support, or is using the ConsoleKit PAM module"
}

