# Copyright 2011 Paul Seidler
# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson \
    systemd-service \
    option-renames [ renames=[ 'systemd providers:systemd' ] ]

SUMMARY="D-Bus interfaces for querying and manipulating user account information"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/AccountsService"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    doc
    gobject-introspection
    gtk-doc
    ( providers: elogind systemd ) [[
        *description = [ Login daemon provider ]
        number-selected = exactly-one
    ]]
"

BUGS_TO="spoonb@exherbo.org"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        doc? (
            app-text/docbook-xml-dtd:4.1.2
            app-text/xmlto
            dev-libs/libxslt
        )
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        sys-apps/dbus
        sys-auth/polkit:1
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.12] )
        providers:systemd? ( sys-apps/systemd[>=186] )
        providers:elogind? ( sys-auth/elogind[>=229.4] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Add-paludisbuild-to-excluded-users.patch
    "${FILES}"/0002-Musl-libc-does-not-support-fgetspent_r-so-fall-back-.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    -Dadmin-group=wheel
    -Duser-heuristics=false
    -Dminimum_uid=1000
    -Dsystemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc docbook'
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'providers:elogind elogind'
    'providers:systemd systemd'
)

src_install() {
    meson_src_install

    keepdir /var/lib/AccountsService/{users,icons}
}

