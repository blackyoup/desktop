# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github cmake python [ python_dep=2.7 ]

SUMMARY="A library that provides automatic proxy configuration management"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gnome [[ description = [ Add support for using GNOME3 settings to determine the correct proxy ] ]]
    kde [[ description = [ Add support for using KDE settings to determine the correct proxy ] ]]
    networkmanager [[ description = [ Add support for using NetworkManager to check if the network topology changed ] ]]
    python
    webkit [[ description = [ Use the webkit javascript engine for PAC ] ]]
    xulrunner [[ description = [ Use the xulrunner javascript engine for PAC ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/libmodman[>=2.0]
        gnome? ( dev-libs/glib:2[>=2.26] )
        networkmanager? (
            net-apps/NetworkManager
            sys-apps/dbus
        )
        webkit? ( net-libs/webkit:= )
        xulrunner? ( dev-libs/spidermonkey:60 )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DFORCE_SYSTEM_LIBMODMAN:BOOL=TRUE
    -DSHARE_INSTALL_DIR:PATH=/usr/share
    -DWITH_DOTNET:BOOL=FALSE
    -DWITH_GNOME2:BOOL=FALSE
    -DWITH_PERL:BOOL=FALSE
    -DWITH_PYTHON3:BOOL=FALSE
    -DWITH_VALA:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'python PYTHON2'
    'gnome GNOME3'
    'kde KDE'
    'networkmanager NM'
    'webkit WEBKIT'
    'webkit WEBKIT3'
    'xulrunner MOZJS'
)

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/f594720280b2e40d81fa6e286a0ef8868687ef7e.patch
    "${FILES}"/95.patch
)

