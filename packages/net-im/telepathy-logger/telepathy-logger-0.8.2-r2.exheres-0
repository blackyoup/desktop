# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require utf8-locale

SUMMARY="Logging infrastructure for telepathy"
HOMEPAGE="https://telepathy.freedesktop.org/wiki"
DOWNLOADS="https://telepathy.freedesktop.org/releases/${PN}/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="0.2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=2.5]
        dev-libs/libxslt
        dev-util/intltool[>=0.35.0]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.28]
        dev-libs/libxml2:2.0
        net-im/telepathy-glib[>=0.19.2][gobject-introspection?]
        dev-db/sqlite:3
        sys-apps/dbus[>=1.1.0]
        dev-libs/dbus-glib:1[>=0.82]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        !net-im/telepathy-logger:0.1 [[
            description = [ slot was moved ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-sync-tools-with-tp-glib-master.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
)

DEFAULT_SRC_TEST_PARAMS=( -j1 )

pkg_setup() {
    # python 3.7 forces an UTF-8 locale so the line below can be dropped if
    # it's our oldest python3 ABI.
    require_utf8_locale
}

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS

    esandbox allow_net "unix-abstract:/tmp/dbus-*"

    XDG_CACHE_HOME="${TEMP}" \
    default

    esandbox disallow_net "unix-abstract:/tmp/dbus-*"
}

