# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="GLib ICE implementation"
HOMEPAGE="https://nice.freedesktop.org/wiki"
DOWNLOADS="https://nice.freedesktop.org/releases/${PNV}.tar.gz"

LICENCES="( LGPL-2.1 MPL-1.1 )"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gstreamer
    gtk-doc
    gupnp
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.48]
        gstreamer? ( media-libs/gstreamer:1.0[>=0.11.91][gobject-introspection?] )
        gupnp? ( net-libs/gupnp-igd:1.0[>=0.2.4][gobject-introspection?] )
        providers:gnutls? ( dev-libs/gnutls[>=2.12.0] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

src_configure() {
    local myconf=(
        --disable-static
        --disable-static-plugins
        --without-gstreamer-0.10
        $(option_enable gobject-introspection introspection)
        $(option_enable gtk-doc)
        $(option_enable gupnp)
        $(option_with gstreamer)
    )

    myconf+=(
        --with-crypto-library=$(option providers:gnutls gnutls openssl)
    )

    econf "${myconf[@]}"
}

